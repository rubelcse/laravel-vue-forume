import Vue from 'vue';
import VueRouter from 'vue-router';
import Parallex from '../components/Parallex.vue';
import Login from '../components/Login/Login.vue';
import Signup from '../components/Login/Signup.vue';
import Logout from '../components/Login/Logout.vue';
import Forum from '../components/Forum/Forum.vue';
import Read from '../components/Forum/Read.vue';
import Create from '../components/Forum/Create.vue';
import CreateCategory from '../components/Category/createCategory.vue';

Vue.use(VueRouter);


// 2. Define some routes
const routes = [
    { path: '/', component: Parallex },
    { path: '/login', component: Login },
    { path: '/logout', component: Logout },
    { path: '/sign-up', component: Signup },
    { path: '/forum', name: 'forum', component: Forum },
    { path: '/ask', component: Create },
    { path: '/category', component: CreateCategory },
    { path: '/question/:slug', name: 'read', component: Read }
];

// 3. Create the router instance and pass the `routes` option
const router = new VueRouter({
    routes : routes,
    hashbang : false,
    mode : 'history'
});

export default router;